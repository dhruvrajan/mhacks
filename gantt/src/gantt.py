import nltk
from grammar_parser import *
import string
import nltk



OLD_GRAMMAR = """
S -> CC
CC -> NP VP | CC Conj CC | SC CC | CC SC
SC -> SConj CC
PP -> P NP
NP -> Det N | Det N PP | 'I'
VP -> V NP | VP PP | VP Conj VP
Det -> 'an' | 'my'
Conj -> 'and' | 'or'
SConj -> 'while' | 'before'
N -> 'elephant' | 'pajamas'
V -> 'shot' | 'wore'
P -> 'in'
"""

GANTT_GRAMMAR = """
S -> CC
CC -> NP VP | CC Conj CC | SC CC | CC SC
SC -> SConj CC
PP -> P NP
NP -> Det N | Det N PP | N
VP -> V NP | V PP | VP Conj VP

V -> 'shot' | 'wore' | 'mow' | 'do' | 'watch' | 'fly' | 'run' | 'go' | 'visit' | 'give'

Det -> 'an' | 'my' | 'the' | 'a'
Conj -> 'and' | 'or'
SConj -> 'while' | 'before' | 'after' | 'if'

N -> N N | N Ger| N Ger PP | 'lawn' | 'I' | 'i' | 'laundry' | 'bats' | 'them' | 'lake' | 'store' | 'movie' | 'Charlie' | 'him' | 'gift' | 'me'
Ger -> 'fly'
HV -> 'must' | 'can'
P -> 'in' | 'like' | 'to' | 'around'
"""


class Task(object):
    def __init__(self, dependency, description: str):
        self.dependency = dependency
        self.description = description

def draw_span(text):
    return """
    <span class="box">
        {}
    </span>
    """.format(text)


def draw_div(text):
    return """
    <div class="level">
        {}
    </div>
    """.format(text)

def draw_ul(l):
    s = """
    <ul>
    """
    for e in l:
        s += "<li>" + e + "</li>"

    s += "</ul>"
    return s


def collapse(cc):
    clauses = []
    # print(cc)
    if cc._label == "CC":
        if len(cc) == 3 and cc[1]._label == "Conj":
            for entity in cc:
                if entity._label == "CC":
                    clauses.append(collapse(entity))
        else:
            clauses.append(cc)

    elif cc._label == "VP":
        # print(clauses)
        if len(cc) == 3 and cc[1]._label == "Conj":
            for entity in cc:
                if entity._label == "VP":
                    clauses.append(collapse(entity))
        else:
            clauses.append(cc)

    return clauses


def find_verb_phrases(tree):
    tasks = []
    for clause in tree:
        print(clause)
        if clause._label == "CC" and len(clause) == 2:
            if clause[0]._label == "SC":
                # print(len(collapse(clause[1][1])))
                CC = [get_terminals(x, []) for x in collapse(clause[1][1]) if x._label != "Conj" and not "N" in x._label]
                conj = get_terminals(clause[0][0], [])
                # conj = [get_terminals(x, []) for x in clause[0][0]]
                sub_cc = [get_terminals(x, []) for x in clause[0][1] if x._label != "Conj"and not "N" in x._label]
                print(sub_cc)
                # CC = get_terminals(clause[1][1], [])
                # conj = get_terminals(clause[0][0], [])
                # sub_cc = get_terminals(clause[0][1], [])

            elif clause[1]._label == "SC":
                print(len(collapse(clause[0][1])))
                CC = [get_terminals(x, []) for x in collapse(clause[0][1]) if x._label != "Conj"and not "N" in x._label]
                conj = get_terminals(clause[1][0], [])
                sub_cc = [get_terminals(x, []) for x in clause[1][1] if x._label != "Conj"and not "N" in x._label]

                # CC = get_terminals(clause[0][1], [])
                # conj = get_terminals(clause[1][0], [])
                # sub_cc = get_terminals(clause[1][1], [])
            print(conj[0])
            if conj[0] == "before":
                print("First, " + (" " + conj[0] + " ").join([" ".join(x) for x in CC]) + " then " + " ".join([" ".join(x) for x in sub_cc]))
                s = draw_span(draw_ul([" ".join(c) for c in CC])) + draw_span(draw_ul([" ".join(c) for c in sub_cc]))
                # print("\n".join([draw_div(" ".join(x)) for x in CC]))
                return s

            if conj[0] == "after" or conj[0] == "if":
                print("First, " + (" " + conj[0] + " ").join([" ".join(x) for x in sub_cc]) + " then " + " ".join([" ".join(x) for x in CC]))
                # print("First, " + " ".join(sub_cc) + " then " + " ".join(CC))
                # s = draw_span(" ".join(sub_cc)) + draw_span(" ".join(CC))
                s = draw_span(draw_ul([" ".join(c) for c in sub_cc])) + draw_span(draw_ul([" ".join(c) for c in CC]))
                return s


def get_terminals(tree, res):
    # print("res: ", res)
    for subtree in tree:
        if type(subtree) == str:
            res.append(subtree)
        else:
            res = get_terminals(subtree, res)
    return res


def get_text(filepath):
    with open(filepath) as f:
        return f.read()


# def draw_chart():
#     s = """"""
#     s += """
#     <!DOCTYPE html>
#     <html>
#     <style>
#         .level {
#             background-color: lightgrey;
#             border: 5px solid green;
#             padding 5px;
#             margin 15px;
#         }
#
#         .level {
#             border: 5px black;
#             padding 5px;
#             margin 15px;
#         }
#     </style>
#
#     <body>
#     """
#
#     s += """
#     </body>
#     </html>
#     """
#
#     print(s)


def main():
    text = get_text("in.txt")
    sentences = nltk.sent_tokenize(text)
    for x in range(len(sentences)):
        sentences[x] = nltk.word_tokenize(sentences[x])

    # Strip punctuation
    sentences = [list(filter(lambda s: s.isalpha(), sentence)) for sentence in sentences]

    parser = nltk.ChartParser(nltk.CFG.fromstring(GANTT_GRAMMAR))

    s = """"""
    s += """
    <!DOCTYPE html>
    <html>
    <style>
        .level {
            background-color: lightgrey;
            width: 300px;
            border: 5px solid green;
            padding: 5px;
            margin: 15px;
        }
        ul {
            float: left;
        }
        li {
            background-color: lightgrey;
            margin: 15px;
        }
    </style>

    <body>
    """

    for sentence in sentences:
        tree = list(parser.parse(sentence))[0]

        # print(tree)

        s += find_verb_phrases(tree)

    s += """
    </body>
    </html>
    """

    # print(s)
    with open("html_out.html", "w") as f:
        f.write(s)


if __name__ == "__main__":
    main()
